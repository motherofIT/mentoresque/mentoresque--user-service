export ENVIRONMENT=dev
docker-compose --env-file $HOME/env/mentoresque.$ENVIRONMENT.env up --build -d --remove-orphans
