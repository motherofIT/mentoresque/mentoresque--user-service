FROM node:current-alpine
LABEL vendor="asyavee"

RUN mkdir -p /app

ENV APP_PATH=/app \
    PORT=3000

WORKDIR $APP_PATH
COPY . .

RUN npm cache verify ;\
    npm install --silent --progress=false --production ;

EXPOSE $PORT
CMD [ "npm", "start" ]
