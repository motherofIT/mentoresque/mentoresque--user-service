const { sendEmail } = require('./index');

const sendVerificationEmail = async (user, req, res) => {
  try {
    const token = user.generateVerificationToken();

    // Save the verification token
    await token.save();

    const link =
      'http://' + req.headers.host + '/api/auth/verify/' + token.token;
    const text = `<p>Please click on the following <a href="${link}">link</a> to verify your account.</p> 
                  <br><p>If you did not request this, please ignore this email.</p>`;

    const message = {
      to: user.email,
      subject: 'Mentoresque Account verification link',
      html: text,
    };

    await sendEmail({ message }).then(() =>
      res.status(200).json({ message: 'Verification link sent' })
    );
  } catch (err) {
    res.status(err.statusCode || 500).json(err);
  }
};

module.exports = { sendVerificationEmail };
