const Email = require('email-templates');
const config = require('../config');

const sendEmail = async ({ template, message, locals }) => {
  const email = new Email({
    send: true,
    message: {
      from: `"Mentoresque" <${config.mailer.from}>`,
    },
    transport: {
      service: 'Yandex',
      auth: {
        user: config.mailer.user,
        pass: config.mailer.pass,
      },
    },
  });

  try {
    await email.send({ template, message, locals }).then(console.log);
  } catch (err) {
    throw err;
  }
};

module.exports = { sendEmail };
