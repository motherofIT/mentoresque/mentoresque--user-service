module.exports = {
  app: {
    port: process.env.MENTORESQUE_PORT,
  },
  db: {
    host: process.env.MENTORESQUE_DB_HOST,
  },
  mailer: {
    user: process.env.MENTORESQUE_MAILER_USER,
    pass: process.env.MENTORESQUE_MAILER_PASSWORD,
    from: process.env.MENTORESQUE_MAILER_FROM,
  },
};
