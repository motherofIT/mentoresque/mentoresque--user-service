require('dotenv').config();

const cors = require('cors');
const morgan = require('morgan');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const express = require('express');
const mongoose = require('mongoose')
const config = require('./config');

mongoose.promise = global.Promise;
const app = express();

mongoose.connect(config.db.host, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
mongoose.set('debug', true)

require('./db/models/User');
require('./config/passport');

// The request handler must be the first middleware on the app
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// All controllers should live here
app.use(cors({origin: true}))
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(
  session({
    secret: 'passport-tutorial',
    cookie: { maxAge: 60000 },
    resave: false,
    saveUninitialized: false,
  })
);
app.use('/', require('./routes'))
app.get('/', (req, res) => {
  res.end('Hello world!');
});

app.use((err, req, res) => {
  res.status(err.status || 500);

  res.json({
    errors: {
      message: err.message,
      error: {},
    },
  });
});

app.listen(config.app.port, () => {
  console.log(`Server is listening on http://localhost:${config.app.port}`)
});
