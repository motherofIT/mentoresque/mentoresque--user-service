const createError = require('http-errors');
const ObjectId = require('mongoose').Types.ObjectId;
const mongoose = require('mongoose');
const User = mongoose.model('Users');
const Token = mongoose.model('Tokens');
const { ROLES, AVAILABILITY_LIST } = require('../db/models/User/const');
const { ERRORS } = require('./const');

const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const emailRegExp = new RegExp(re);

module.exports.validateCreateUser = async user => {
  const { email } = user;
  const exist = await User.findOne({ email }).then(
    res => res && res.length !== 0
  );
  if (exist) {
    throw new createError(409, ERRORS.email.notunique(email));
  }
  return;
};

module.exports.validateCredentials = async user => {
  const { email, password } = user;
  const valid = emailRegExp.test(email);

  if (!email) {
    throw new createError(400, ERRORS.email.required);
  }
  if (!valid) {
    throw new createError(400, ERRORS.email.invalid);
  }
  if (!password) {
    throw new createError(400, ERRORS.password.required);
  }
  return;
};

module.exports.validateUserDelete = async id => {
  const exist = await User.getUserById(id).then(res => res && res.length !== 0);
  if (!id) {
    throw new createError(400, ERRORS.id.required);
  }
  if (new ObjectId(id).toString() !== id) {
    throw new createError(400, ERRORS.id.invalid);
  }
  if (!exist) {
    throw new createError(404, ERRORS.id.notfound(id));
  }
  return;
};

module.exports.validateUserUpdate = async params => {
  const {
    id,
    email,
    role,
    time_available,
    registered,
    updated,
    skills,
    active,
  } = params;

  if (!ObjectId.isValid(id) || new ObjectId(id).toString() !== id) {
    throw new createError(400, ERRORS.id.invalid);
  }

  const exist = User.getUserById(id).then(res => res && res.length !== 0);

  if (!exist) {
    throw new createError(404, ERRORS.id.notfound(id));
  }

  if (email) {
    const validEmail = emailRegExp.test(email);
    const emailExist = await User.getUserIdByEmail({ email }).then(
      res => res && res.length !== 0
    );

    if (!validEmail) {
      throw new createError(400, ERRORS.email.invalid);
    }
    if (emailExist) {
      throw new createError(409, ERRORS.email.notunique(email));
    }
    return;
  }

  if (registered || updated) {
    throw new createError(400, ERRORS.timestamp.readonly);
  }
  if (role && ROLES.indexOf(role) === -1) {
    throw new createError(400, ERRORS.role.invalid(role));
  }
  if (time_available && AVAILABILITY_LIST.indexOf(time_available) === -1) {
    throw new createError(400, ERRORS.time_available.invalid(time_available));
  }
  if (skills && !Array.isArray(skills)) {
    throw new createError(400, ERRORS.skills.invalid(typeof skills));
  }
  if (active && typeof active !== 'boolean') {
    throw new createError(400, ERRORS.active.invalid(typeof active));
  }
  return;
};

module.exports.validateVerificationToken = async token => {
  const userToken = await Token.findOne({ token });

  if (!userToken) {
    throw new createError(400, ERRORS.token.invalid);
  }
  return userToken;
};
