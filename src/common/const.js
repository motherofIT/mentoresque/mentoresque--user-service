module.exports.ERRORS = {
  email: {
    required: "Parameter 'email' is required",
    invalid: 'Invalid email',
    notfound: email => `User with email: '${email}' does not exist.`,
    notunique: email =>
      `User with email: '${email.toLowerCase()}' already exist. Email should be unique`,
  },
  password: {
    required: "Parameter 'password' is required",
  },
  id: {
    required: "Parameter 'id' is required",
    notfound: id => `User with id: '${id}' does not exist.`,
    readonly: "Can't rewrite id.",
    invalid: 'Id format invalid',
  },
  timestamp: {
    readonly: "Can't rewrite timestamps.",
  },
  role: {
    invalid: role =>
      `Parameter "role" should be either 'jedi' or 'padawan', received ${role} instead`,
  },
  time_available: {
    invalid: data =>
      `Parameter "time_available" is incorrect: should be either 0, 1 or 5, received ${data} instead`,
  },
  skills: {
    invalid: type =>
      `Parameter "skills" is incorrect: should be an array, received ${type} instead`,
  },
  active: {
    invalid: type =>
      `Parameter "active" is incorrect: should be boolean, received ${type} instead`,
  },
  token: {
    invalid: 'Unable to find token',
  },
};
