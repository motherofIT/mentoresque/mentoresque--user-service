const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  res.json({ alive: 'it is alive!' });
});

module.exports = router;
