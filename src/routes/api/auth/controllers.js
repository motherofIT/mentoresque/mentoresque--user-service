const mongoose = require('mongoose');
const passport = require('passport');
const { sendEmail } = require('../../../mailer');
const { validateCredentials, validateVerificationToken } = require('../../../common/validators');
const { sendVerificationEmail } = require('../../../mailer/verificationEmail');
const User = mongoose.model('Users');

exports.login = async (req, res, next) => {
  try {
    const { user } = req.body;

    await validateCredentials(user)

    return passport.authenticate(
      'local',
      { session: false },
      (err, passportUser, info) => {
        if (err) {
          return next(err);
        }

        if (passportUser) {
          const user = passportUser;
          user.token = passportUser.generateJWT();

          return res.json({ user: user.toAuthJSON() });
        }

        return res.status(400).json(info);
      }
    )(req, res, next);
  } catch (err) {
    res.status(err.statusCode || 500).json(err);
  }
}

exports.verify = async (req, res) => {
  try {
    const { params: { token } } = req
    const userToken = await validateVerificationToken(token)
    User.findOne({ _id: userToken.userId }, (err, user) => {
      if (!user) {
        return res.status(400).json({ id: user._id, message: 'Can\'t find user' })
      }
      if (user.active) {
        return res.status(400).json({ id: user._id, message: 'Account is already active' })
      }
      user.active = true;
      user.save().then(() => res.status(201).json({ message: 'Account has been verified', user: user.toAuthJSON() }))
    })

  } catch (err) {
    res.status(err.statusCode || 500).json(err);
  }
}

exports.resendToken = async (req, res) => {
  try {
    const { email } = req.body.user;

    const user = await User.findOne({ email });

    if (!user) {
      return res.status(401).json({ message: 'The email address ' + email + ' does not exist'});
    }

    if (user.active) {
      return res.status(400).json({ message: 'Account is already active'});
    }

    await sendVerificationEmail(user, req, res);

  } catch (err) {
    res.status(err.statusCode || 500).json(err);
  }
};

exports.recover = async (req, res) => {
  try {
    const { email } = req.body.user
    const user = await User.findOne({ email });
    if (!user) {
      res.status(404).json(`Email ${email} doesn't exist`)
    }
    user.generatePasswordReset();
    await user.save();
    const link = "http://" + req.headers.host + "/api/auth/reset/" + user.resetPasswordToken;
    const message = {
      to: user.email,
      subject: 'Mentoresque password recovery',
      text: `Here's your link to recover: ${link}`
    }
    await sendEmail({message}).then(() => res.status(200).json({ message: `A reset link has been sent to ${user.email}` }))
  } catch (err) {
    res.status(err.statusCode || 500).json(err);
  }
}

exports.reset = async (req, res) => {
  try {
    const { token } = req.params;

    const user = await User.findOne({ resetPasswordToken: token, resetPasswordExpires: {$gt: Date.now()} });

    if (!user) {
      return res.status(401).json({message: 'Password reset token is invalid or has expired.'});
    }

    return res.status(202).json({ message: 'Token accepted' })

  } catch (err) {
    res.status(err.statusCode || 500).json(err);
  }
}

exports.resetPassword = async (req, res) => {
  try {
    const { token } = req.params;

    const user = await User.findOne({ resetPasswordToken: token, resetPasswordExpires: {$gt: Date.now()} });

    if (!user) return res.status(401).json({ message: 'Password reset token is invalid or has expired.' });

    user.password = req.body.user.password;
    user.resetPasswordToken = undefined;
    user.resetPasswordExpires = undefined;
    user.isVerified = true;

    // Save the updated user object
    await user.save();

    const message = {
      to: user.email,
      subject: 'Your password has been changed',
      text: `This is a confirmation that the password for your account ${user.email} has just been changed.`,
    }

    await sendEmail({ message });

    res.status(200).json({ message: 'Your password has been updated.' });

  } catch (error) {
    res.status(500).json({ message: error.message })
  }
};
