const express = require('express');
const router = express.Router();
const controllers = require('./controllers');
const auth = require('../../../common/helpers/auth');

router.get('/', auth.optional, (req, res) => res.status(200).json({ message: 'Auth route' }));

router.post('/login', auth.optional, controllers.login);
router.post('/recover', auth.optional, controllers.recover);
router.get('/reset/:token', auth.optional, controllers.reset);
router.post('/reset/:token', auth.optional, controllers.resetPassword);

router.post('/verify/resend', auth.optional, controllers.resendToken);
router.post('/verify/:token', auth.optional, controllers.verify);

module.exports = router;
