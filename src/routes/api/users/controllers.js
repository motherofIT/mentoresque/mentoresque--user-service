// const fs = require('fs');

// const USER = require('../../db/models/User')
// const { ERRORS } = require('../../common/constants')
// const createError = require('http-errors');
// const { validateUserCreate, validateUserUpdate, validateUserDelete } = require('../../common/validators');
// const { sendEmail } = require('../../mailer');

// const list = async (req, res) => {
//   try {
//     const users = await USER.getAllUser();
//     res.status(200).json(users)
//   }
//   catch (err) {
//     res.json(err)
//   }
// };
//
// const retrieve = async (req, res) => {
//   const params = {
//     email: req.query.email
//   };
//   try {
//     const user = await USER.getUserByEmail(params);
//     if (!user) {
//       res.status(404)
//       throw new createError(404, ERRORS.email.notfound(params.email));
//     }
//     if (user) {
//       res.status(200).json(user)
//     }
//   }
//   catch (err) {
//     res.json(err)
//   }
// };
//
// const create = async (req, res) => {
//   const params = {
//     email: req.body.email
//   };
//   try {
//     await validateUserCreate(params)
//     let user = await USER.createUser(params);
//     user = user.toJSON();
//     delete user.__v;
//     delete user.password
//     res.status(201).json(user);
//
//     await sendEmail({
//       message, //TODO
//       template, //TODO
//       locals //TODO
//     });
//   }
//   catch (err) {
//     res.status(err.statusCode || 500).json(err)
//   }
// }
//
// const destroy = async (req, res) => {
//   const id = req.params.id
//   try {
//     await validateUserDelete(id)
//     await USER.deleteUser(id);
//     res.status(200).json({
//       statusCode: 200,
//       message: `User id: ${id} was successfully deleted`
//     });
//   }
//   catch (err) {
//     res.status(err.statusCode || 500).json(err)
//   }
// }
//
// const update = async (req, res) => {
//   const params = req.body
//   const id = req.params.id
//   try {
//     await validateUserUpdate({ id, ...params})
//     const update = await USER.updateUser({ id, ...params})
//     res.status(200).json(update);
//   }
//   catch (err) {
//     console.log(err)
//     res.status(err.statusCode || 500).json(err)
//   }
// }
//
// module.exports = { list, retrieve, create, destroy, update };

const mongoose = require('mongoose');
const { validateCreateUser, validateCredentials, validateVerificationToken, validateUserUpdate, validateUserDelete } = require('../../../common/validators');
const { sendVerificationEmail } = require('../../../mailer/verificationEmail')
const User = mongoose.model('Users');

exports.create = async (req, res) => {
  try {
    const {
      body: { user },
    } = req;

    await validateCredentials(user);
    await validateCreateUser(user);

    const newUser = new User(user);
    newUser.setPassword(user.password);

    const _user = await newUser.save();
    await sendVerificationEmail(_user, req, res)
  } catch (err) {
    res.status(err.statusCode || 500).json(err);
  }
};

exports.current = (req, res) => {
  try {
    const { payload: { id }, } = req;

    return User.findById(id).then(user => {
      if (!user) {
        return res.sendStatus(403);
      }

      return res.json({ user: user.toAuthJSON() });
    });
  } catch (err) {
    res.status(err.statusCode || 500).json(err);
  }
}
