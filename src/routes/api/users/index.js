const express = require('express');
const router = express.Router();
const controllers = require('./controllers');
const auth = require('../../../common/helpers/auth');

router.post('/', auth.optional, controllers.create);
router.get('/current', auth.required, controllers.current);

// router.get('/users', users.list)
// router.get('/user', users.retrieve)
// router.delete('/user/:id', users.destroy)
// router.put('/user/:id', users.update)

module.exports = router;
