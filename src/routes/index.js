const express = require('express');
const alive = require('./alive');
const api = require('./api');
const router = express.Router();

router.use('/api', api);
router.use('/alive', alive);

module.exports = router;
