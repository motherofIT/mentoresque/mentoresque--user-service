const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const Token = require('../Token');

const UserSchema = new mongoose.Schema(
  {
    hash: String,
    salt: String,
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      set: email => email.toLowerCase(),
    },
    grants: {
      type: [String],
    },
    resetPasswordToken: {
      type: String,
      required: false,
    },
    resetPasswordExpires: {
      type: Date,
      required: false,
    },
    active: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

UserSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto
    .pbkdf2Sync(password, this.salt, 10000, 512, 'sha512')
    .toString('hex');
};

UserSchema.methods.validatePassword = function(password) {
  const hash = crypto
    .pbkdf2Sync(password, this.salt, 10000, 512, 'sha512')
    .toString('hex');
  return this.hash === hash;
};

UserSchema.methods.generatePasswordReset = function() {
  this.resetPasswordToken = crypto.randomBytes(20).toString('hex');
  this.resetPasswordExpires = Date.now() + 3600000; // expires in an hour
};

UserSchema.methods.generateJWT = function() {
  const today = new Date();
  const expirationDate = new Date(today);
  expirationDate.setDate(today.getDate() + 60);
  const payload = {
    email: this.email,
    id: this._id,
  };
  return jwt.sign(payload, process.env.MENTORESQUE_JWT_SECRET, {
    expiresIn: parseInt(expirationDate.getTime() / 1000, 10),
  });
};

UserSchema.methods.toAuthJSON = function() {
  return {
    id: this._id,
    email: this.email,
    grants: this.grants,
    token: this.generateJWT(),
  };
};

UserSchema.methods.generateVerificationToken = function() {
  const payload = {
    userId: this._id,
    token: crypto.randomBytes(16).toString('hex'),
  };

  return new Token(payload);
};

// UserSchema.methods.getAllUsers = () => {
//   const res = this.find({}, '-__v -password');
//   return res;
// };
//
// UserSchema.methods.getUserIdByEmail = mail => {
//   const res = this.findOne(mail, 'id email');
//   return res;
// };
//
// UserSchema.methods.getUserById = id => {
//   const res = this.findById(id);
//   return res;
// };
//
// UserSchema.methods.getUserByEmail = mail => {
//   const res = this.findOne(mail, '-__v -password');
//   return res;
// };
//
// UserSchema.methods.deleteUser = id => {
//   const res = this.findByIdAndDelete(id);
//   return res;
// };

mongoose.model('Users', UserSchema);
