const ROLE = {
  JEDI: 'jedi',
  PADAWAN: 'padawan',
};

module.exports.ROLES = [ROLE.JEDI, ROLE.PADAWAN];

const AVAILABILITY = {
  LOW: 0,
  MEDIUM: 1,
  HIGH: 5,
};

module.exports.AVAILABILITY_LIST = [
  AVAILABILITY.LOW,
  AVAILABILITY.MEDIUM,
  AVAILABILITY.HIGH,
];
